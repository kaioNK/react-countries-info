import React, {Component} from 'react'
import ListData from '../data/country.json'
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'

export default class ListCountries extends Component{

  constructor(props){
    super(props)
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    }
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  render(){
    return(
      <div>
        <h1>País</h1>
          <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} size="lg" direction="right">
            <DropdownToggle caret>
              Localidades
            </DropdownToggle>
            <DropdownMenu
               modifiers={{
                setMaxHeight: {
                  enabled: true,
                  order: 890,
                  fn: (data) => {
                    return {
                      ...data,
                      styles: {
                        ...data.styles,
                        overflow: 'auto',
                        maxHeight: 500,
                      },
                    };
                  },
                },
              }}
            >
            {ListData.map((info, index) => {
              return (<DropdownItem>{info.country}</DropdownItem>)
            })}          
            </DropdownMenu>
          </Dropdown>
      </div>
    )
  }
}