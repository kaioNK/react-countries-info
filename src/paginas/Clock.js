import React, {Component} from 'react'
import ListCountries from './ListCountries'
import {Container} from 'reactstrap'

const monthName = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 
'setembro', 'outubro', 'novembro', 'dezembro']

const monthNumber = [1,2,3,4,5,6,7,8,9,10,11,12]

export default class Clock extends Component{

  constructor(props){
    super(props)
    this.state = {
      date: new Date(),
      month: '',
      day: '',      
    }
  }

  componentDidMount(){
    this.timerID = setInterval(
      () => this.tick(), 1000
    )
  }

  componentWillUnmount(){
    clearInterval(this.timerID)
  }

  tick(){
    this.setState({
      date: new Date()  
    })
  }

  render(){
    return(
      <Container>
        <h1>Hello World!</h1>
        <h2>Horário: {this.state.date.toLocaleTimeString()}</h2>
        <h2>Data: {`${this.state.date.getDate()}/${monthNumber[this.state.date.getMonth()]}/${this.state.date.getFullYear()}`}</h2>
        <hr/>
        
        <ListCountries />
        
      </Container>
    )
  }
}